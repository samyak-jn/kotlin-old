// usages in build scripts are not tracked properly
@file:Suppress("unused")

import groovy.lang.Closure
import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.api.file.CopySourceSpec
import org.gradle.api.file.SourceDirectorySet
import org.gradle.api.plugins.JavaPluginConvention
import org.gradle.api.plugins.BasePluginConvention
import org.gradle.api.tasks.SourceSet
import org.gradle.api.tasks.SourceSetContainer
import org.gradle.api.tasks.SourceSetOutput
import java.io.File
import java.util.concurrent.Callable

//inline fun <reified T : Task> Project.task(noinline configuration: T.() -> Unit) = tasks.create(T::class, configuration)

fun Project.callGroovy(name: String, vararg args: Any?): Any? {
    return (property(name) as Closure<*>).call(*args)
}
fun customCallGroovy(project:Project,name: String,args:List<Any>):Any?{return project.callGroovy(name,*args.toTypedArray())}
inline fun <T : Any> Project.withJavaPlugin(crossinline body: () -> T?): T? {
    var res: T? = null
    pluginManager.withPlugin("java") {
        res = body()
    }
    return res
}

fun Project.getCompiledClasses(): SourceSetOutput? = withJavaPlugin { mainSourceSet.output }

fun Project.getSources(): SourceDirectorySet? = withJavaPlugin { mainSourceSet.allSource }

fun Project.getResourceFiles(): SourceDirectorySet? = withJavaPlugin { mainSourceSet.resources }

fun fileFrom(root: File, vararg children: String): File = children.fold(root) { f, c -> File(f, c) }

fun fileFrom(root: String, vararg children: String): File = children.fold(File(root)) { f, c -> File(f, c) }
fun customFileFrom(root: String, children: List<String>): File {return fileFrom(root,*children.toTypedArray())}
fun customFileFrom(root: File, children: List<String>): File {return fileFrom(root,*children.toTypedArray())}
var Project.jvmTarget: String?
    get() = getExtensions().getExtraProperties().takeIf { it.has("jvmTarget") }?.get("jvmTarget") as? String
    set(v) {
        getExtensions().getExtraProperties().set("jvmTarget", v)
    }

var Project.javaHome: String?
    get() = getExtensions().getExtraProperties().takeIf { it.has("javaHome") }?.get("javaHome") as? String
    set(v) {
        getExtensions().getExtraProperties().set("javaHome", v)
    }

/*fun Project.generator(fqName: String, sourceSet: SourceSet? = null) = smartJavaExec {
    classpath = (sourceSet ?: testSourceSet).runtimeClasspath
    main = fqName
    workingDir = rootDir
}

fun customGenerator(project:Project,fqName: String, sourceSet: SourceSet? = null) = project.generator(fqName,sourceSet)*/

fun Project.getBooleanProperty(name: String): Boolean? = this.findProperty(name)?.let {
    val v = it.toString()
    if (v.isBlank()) true
    else v.toBoolean()
}

fun getBooleanPropertyFromProject(project:Project,name: String): Boolean? {
	return project.getBooleanProperty(name)
}

inline fun CopySourceSpec.from(crossinline filesProvider: () -> Any?): CopySourceSpec = from(Callable { filesProvider() })

fun Project.javaPluginConvention(): JavaPluginConvention {return project.getConvention().getPlugin(JavaPluginConvention::class.java)}
fun getTargetName(project:Project):String? {return project.getConvention().getPlugin(BasePluginConvention::class.java).archivesBaseName.removePrefix("kotlin-") + ".jar"}
fun customGetBaseConvention(project:Project):BasePluginConvention {return project.getConvention().getPlugin(BasePluginConvention::class.java)}
