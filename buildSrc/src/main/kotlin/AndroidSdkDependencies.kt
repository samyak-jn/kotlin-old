@file:Suppress("unused") // usages in build scripts are not tracked properly

import org.gradle.api.Project
import org.gradle.api.artifacts.dsl.RepositoryHandler
import org.gradle.api.artifacts.repositories.IvyArtifactRepository
import java.io.File

fun androidDxJarRepo(repositoryHandler: RepositoryHandler,project: Project): IvyArtifactRepository = repositoryHandler.ivy {
    val baseDir = File("${project.rootDir}/buildSrc/prepare-deps/android-dx/build/repo")
    ivyPattern("${baseDir.canonicalPath}/[organisation]/[module]/[revision]/[module].ivy.xml")
    artifactPattern("${baseDir.canonicalPath}/[organisation]/[module]/[revision]/[artifact](-[classifier]).jar")
}
//val rootProject:Project = null;
fun Project.androidDxJar():String { 
	val consta:String = rootProject.extensions.getExtraProperties().get("versions.androidBuildTools").toString()
	return "kotlin.build.custom.deps:android-dx".toString()
}
fun customAndroidDxJar(project:Project) =project.androidDxJar() 

